# contact-form

Formulaire de contacte pour le site de katzei

# installation

To install you need to:

* install flask, jinja2 an sqlalchemy
* clone the repo in the /site directory
* copy the etc content in /etc
* link /etc/contact-form/contact-form.service to /etc/systemd/system/contact-form.service

By default it will serve on port 9090